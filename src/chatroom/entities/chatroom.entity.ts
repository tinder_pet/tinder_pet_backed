import { Message } from 'src/message/entities/message.entity';
import { User } from 'src/user/entities/user.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';

@Entity()
export class ChatRoom {
  @PrimaryGeneratedColumn()
  roomID: number;

  @ManyToOne(() => User, (user) => user.chatRooms)
  user1: User;

  @ManyToOne(() => User, (user) => user.chatRooms)
  user2: User;

  @OneToMany(() => Message, (message) => message.chatRoom)
  messages: Message[];
}
