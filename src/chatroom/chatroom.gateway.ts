import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({
  namespace: '/chat',
  cors: {
    origin: '*',
  },
})
export class ChatGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer() server: Server;

  afterInit(server: Server) {
    console.log('Chat WebSocket server initialized');
  }

  async handleConnection(client: Socket) {
    const chatRoomID = client.handshake.query.chatRoomID;

    const roomID = parseInt(
      Array.isArray(chatRoomID) ? chatRoomID[0] : chatRoomID,
      10,
    );

    if (isNaN(roomID)) {
      console.log(`Invalid chatRoomID: ${chatRoomID}`);
      client.disconnect();
      return;
    }

    client.join(`chatroom_${roomID}`);
    console.log(`Client connected to chat room ${roomID}: ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }
  @SubscribeMessage('sendMessage')
  handleSendMessage(
    client: Socket,
    payload: { chatRoomID: number; message: string },
  ) {
    const room = `chatroom_${payload.chatRoomID}`;
    this.server.to(room).emit('receiveMessage', {
      message: payload.message,
      senderID: client.id,
    });
  }

  @SubscribeMessage('joinChatRoom')
  handleJoinChatRoom(client: Socket, chatRoomID: number) {
    client.join(`chatroom_${chatRoomID}`);
    console.log(`Client joined chat room ${chatRoomID}: ${client.id}`);
  }
}
