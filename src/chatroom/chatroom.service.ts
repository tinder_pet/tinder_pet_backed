import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateChatroomDto } from './dto/create-chatroom.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChatRoom } from './entities/chatroom.entity';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class ChatroomService {
  constructor(
    @InjectRepository(ChatRoom)
    private chatRoomRepository: Repository<ChatRoom>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async createChatroom(
    createChatroomDto: CreateChatroomDto,
  ): Promise<ChatRoom> {
    const user1 = await this.userRepository.findOne({
      where: { userID: createChatroomDto.user1 },
    });
    const user2 = await this.userRepository.findOne({
      where: { userID: createChatroomDto.user2 },
    });

    if (!user1 || !user2) {
      throw new NotFoundException('One or both users not found');
    }

    const chatroom = new ChatRoom();
    chatroom.user1 = user1;
    chatroom.user2 = user2;

    return await this.chatRoomRepository.save(chatroom);
  }

  findAll() {
    return this.chatRoomRepository.find();
  }

  async findOne(id: number): Promise<ChatRoom> {
    const chatroom = await this.chatRoomRepository
      .createQueryBuilder('chatroom')
      .leftJoinAndSelect('chatroom.user1', 'user1')
      .leftJoinAndSelect('chatroom.user2', 'user2')
      .leftJoinAndSelect('user1.pets', 'pets1')
      .leftJoinAndSelect('user2.pets', 'pets2')
      .leftJoinAndSelect('chatroom.messages', 'message')
      .leftJoinAndSelect('message.sender', 'sender')
      .where('chatroom.roomID = :id', { id })
      .getOne();

    if (!chatroom) {
      throw new NotFoundException('Chat room not found');
    }

    // Transform messages to include userID
    const transformedMessages = chatroom.messages.map((message) => {
      const userID = message.sender?.userID;

      return {
        ...message,
        userID: userID !== undefined ? userID : null,
        sender: undefined,
      };
    });

    return {
      ...chatroom,
      messages: transformedMessages,
    };
  }

  async remove(id: number) {
    const chatroom = await this.chatRoomRepository.findOneBy({
      roomID: id,
    });
    if (!chatroom) {
      throw new NotFoundException();
    }
    return this.chatRoomRepository.softRemove(chatroom);
  }
  async findChatroomsByUserId(userID: number): Promise<ChatRoom[]> {
    const chatrooms = await this.chatRoomRepository.find({
      where: [{ user1: { userID } }, { user2: { userID } }],
      relations: [
        'user1',
        'user2',
        'user1.pets',
        'user1.pets.images',
        'user2.pets',
        'user2.pets.images',
        'messages',
      ],
    });

    if (!chatrooms.length) {
      throw new NotFoundException('No chatrooms found for this user');
    }

    return chatrooms;
  }
}
