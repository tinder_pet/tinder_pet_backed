import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { ChatroomService } from './chatroom.service';
import { CreateChatroomDto } from './dto/create-chatroom.dto';

@Controller('chatroom')
export class ChatroomController {
  constructor(private readonly chatroomService: ChatroomService) {}
  @Get('user/:userID/chatrooms')
  async getChatroomsByUser(@Param('userID') userID: number) {
    return this.chatroomService.findChatroomsByUserId(userID);
  }

  @Post()
  create(@Body() createChatroomDto: CreateChatroomDto) {
    return this.chatroomService.createChatroom(createChatroomDto);
  }

  @Get()
  findAll() {
    return this.chatroomService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.chatroomService.findOne(+id);
  }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateChatroomDto: UpdateChatroomDto,
  // ) {
  //   return this.chatroomService.update(+id, updateChatroomDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.chatroomService.remove(+id);
  }
}
