import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { MessageService } from './message.service';
import { Message } from './entities/message.entity';

@Controller('messages')
export class MessageController {
  constructor(private readonly messageService: MessageService) {}
  @Get(':chatRoomID')
  async getMessagesByChatRoom(
    @Param('chatRoomID') chatRoomID: number,
  ): Promise<Message[]> {
    return this.messageService.getMessagesByChatRoom(chatRoomID);
  }

  @Post('send')
  async sendMessage(
    @Body() payload: { content: string; chatRoomID: number; userID: number },
  ): Promise<Message> {
    return this.messageService.createMessage(
      payload.content,
      payload.chatRoomID,
      payload.userID,
    );
  }
}
