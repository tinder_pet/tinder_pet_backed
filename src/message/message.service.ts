import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Message } from './entities/message.entity';
import { Repository } from 'typeorm';
import { ChatRoom } from 'src/chatroom/entities/chatroom.entity';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private messageRepository: Repository<Message>,
    @InjectRepository(ChatRoom)
    private chatRoomRepository: Repository<ChatRoom>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async createMessage(
    content: string,
    chatRoomID: number,
    userID: number,
  ): Promise<Message> {
    const chatRoom = await this.chatRoomRepository.findOne({
      where: { roomID: chatRoomID },
    });

    if (!chatRoom) {
      throw new Error('Chat room not found');
    }

    const sender = await this.usersRepository.findOne({ where: { userID } });

    if (!sender) {
      throw new Error('Sender not found');
    }

    const message = new Message();
    message.content = content;
    message.sentAt = new Date().toISOString();
    message.chatRoom = chatRoom;
    message.sender = sender;
    console.log('Saving message:', message);

    return this.messageRepository.save(message);
  }

  async getMessagesByChatRoom(roomID: number): Promise<Message[]> {
    return this.messageRepository.find({
      where: { chatRoom: { roomID } },
      relations: ['chatRoom', 'sender'],
      order: { sentAt: 'ASC' },
    });
  }
}
