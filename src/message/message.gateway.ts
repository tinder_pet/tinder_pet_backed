import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { MessageService } from './message.service';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class MessageGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer() server: Server;

  constructor(private messageService: MessageService) {}

  afterInit(server: Server) {
    console.log('WebSocket server initialized');
  }

  async handleConnection(client: Socket) {
    const chatRoomID = client.handshake.query.chatRoomID;

    if (chatRoomID) {
      const roomID = parseInt(
        Array.isArray(chatRoomID) ? chatRoomID[0] : chatRoomID,
        10,
      );
      if (!isNaN(roomID)) {
        client.join(`chatroom_${roomID}`);
        console.log(`Client connected to chat room ${roomID}: ${client.id}`);

        const messages =
          await this.messageService.getMessagesByChatRoom(roomID);
        client.emit('previousMessages', messages);
      } else {
        console.log(`Invalid chatRoomID: ${chatRoomID}`);
        client.disconnect();
      }
    } else {
      // console.log(`Client connected for notifications: ${client.id}`);
    }
  }

  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('sendMessage')
  async handleMessage(
    client: Socket,
    payload: { content: string; chatRoomID: number; userID: number },
  ) {
    const { content, chatRoomID, userID } = payload;

    // console.log('Received message payload:');
    // console.log(`Content: ${content}`);
    // console.log(`Chat Room ID: ${chatRoomID}`);
    // console.log(`User ID: ${userID}`);

    if (!chatRoomID) {
      console.log(`Invalid chatRoomID: ${chatRoomID}`);
      client.emit('error', { message: 'Invalid chatRoomID' });
      return;
    }

    try {
      const message = await this.messageService.createMessage(
        content,
        chatRoomID,
        userID,
      );

      // console.log('Sending message to chatroom:');
      // console.log(`Message Content: ${message.content}`);
      // console.log(`Chat Room ID: ${message.chatRoom.roomID}`);
      // console.log(`User ID: ${message.sender.userID}`);

      client.to(`chatroom_${chatRoomID}`).emit('receiveMessage', message);
      this.server.emit('newMessage', {
        chatRoomID,
        message: content,
        userID,
      });
      console.log('Message sent to chat room and notification sent');
    } catch (error) {
      console.error('Error handling message:', error);
      client.emit('error', { message: 'Error processing your message' });
    }
  }

  @SubscribeMessage('joinChatRoom')
  handleJoinChatRoom(client: Socket, chatRoomID: number) {
    client.join(`chatroom_${chatRoomID}`);
    console.log(`Client joined chat room ${chatRoomID}: ${client.id}`);
  }
}
