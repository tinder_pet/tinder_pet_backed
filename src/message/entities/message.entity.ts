import { ChatRoom } from 'src/chatroom/entities/chatroom.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  messageID: number;

  @Column()
  content: string;

  @Column()
  sentAt: string;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => ChatRoom, (chatRoom) => chatRoom.messages)
  chatRoom: ChatRoom;

  @ManyToOne(() => User, (user) => user.messages)
  sender: User;
}
