import { IsInt, IsNotEmpty, isNotEmpty, IsString } from 'class-validator';

export class CreateMessageDto {
  @IsNotEmpty()
  @IsString()
  content: string;

  @IsNotEmpty()
  @IsString()
  sentAt: string;

  @IsNotEmpty()
  @IsInt()
  chatRoomID: number;
}
