import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  async signIn(
    email: string, // เปลี่ยน username เป็น email
    pass: string,
  ): Promise<{ access_token: string; user: any }> {
    const user = await this.usersService.findOneByEmail(email); // ค้นหาผู้ใช้ตามอีเมล

    if (!user || !(await bcrypt.compare(pass, user.password))) {
      console.log(`Login failed for email: ${email}`);
      throw new UnauthorizedException();
    }

    console.log('Login successful:', user);

    const payload = { sub: user.userID, email: user.email }; // payload ที่ใช้สร้าง JWT
    const access_token = await this.jwtService.signAsync(payload); // สร้าง access_token โดยใช้ JWT

    console.log(`Access token generated for user ${user.email}`);
    const { password, ...userWithoutPassword } = user;

    return {
      access_token,
      user: userWithoutPassword,
    };
  }
}
