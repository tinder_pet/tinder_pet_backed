import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class CreatePetDto {
  @IsNotEmpty()
  @IsString()
  petName: string;

  @IsNotEmpty()
  @IsString()
  sex: string;

  @IsNotEmpty()
  @IsNumber()
  weight: number;
  @IsNotEmpty()
  @IsNumber()
  age: number;

  @IsNotEmpty()
  @IsString()
  color: string;
  @IsNotEmpty()
  @IsString()
  species: string; // cat & Dog

  @IsNotEmpty()
  @IsString()
  furLength: string;

  @IsNotEmpty()
  @IsString()
  breed: string;

  @IsNotEmpty()
  @IsString()
  petDes: string;

  @IsNotEmpty()
  @IsString()
  vaccine: string;

  @IsNotEmpty()
  @IsString()
  foodsEaten: string;

  @IsNotEmpty()
  @IsString()
  parentingStatus: string;

  @IsNotEmpty()
  @IsString()
  sterileState: string;

  @IsNotEmpty()
  @IsString()
  healthHistory: string;

  @IsNotEmpty()
  @IsString()
  property: string;

  @IsNotEmpty()
  @IsString()
  specialBehaviors: string;

  @IsNotEmpty()
  @IsString()
  objective: string;

  @IsNotEmpty()
  userID: number;

  images?: string[];
}
