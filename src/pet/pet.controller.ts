import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  ParseIntPipe,
  NotFoundException,
} from '@nestjs/common';
import { PetService } from './pet.service';
import { CreatePetDto } from './dto/create-pet.dto';
import { UpdatePetDto } from './dto/update-pet.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { Pet } from './entities/pet.entity';

@Controller('pet')
export class PetController {
  constructor(private readonly petService: PetService) {}
  @Get('nearby/:userId')
  async findNearbySameSpeciesPets(
    @Param('userId', ParseIntPipe) userId: number,
  ): Promise<{ pet: Pet; distance: string }[]> {
    return this.petService.findNearbySameSpeciesPets(userId);
  }
  @Post('nearby-compatible/:userId')
  async getNearbyCompatiblePets(
    @Param('userId') userId: number,
    @Body('weights')
    weights: {
      breed?: number;
      color?: number;
      age?: number;
      weight?: number;
      furLength?: number;
      behavior?: number;
    },
  ): Promise<{ pet: Pet; distance: string; compatibility: number }[]> {
    if (!userId) {
      throw new NotFoundException('User ID is required');
    }

    return this.petService.findNearbyCompatiblePets(userId, weights);
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './pet_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  async create(
    @Body() createPetDto: CreatePetDto,
    @UploadedFile() file?: Express.Multer.File,
  ) {
    // หากมีไฟล์ จะทำการเพิ่มชื่อไฟล์ลงใน images
    if (file) {
      createPetDto.images = [file.filename];
    }
    return this.petService.create(createPetDto);
  }

  @Get('user/:userId')
  async findPetsByUserId(
    @Param('userId', ParseIntPipe) userId: number,
  ): Promise<Pet[]> {
    return this.petService.findPetsByUserId(userId);
  }

  @Get()
  findAll() {
    return this.petService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.petService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePetDto: UpdatePetDto) {
    return this.petService.update(+id, updatePetDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.petService.remove(+id);
  }
}
