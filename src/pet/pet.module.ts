import { Module } from '@nestjs/common';
import { PetService } from './pet.service';
import { PetController } from './pet.controller';
import { Pet } from './entities/pet.entity';
import { Image } from 'src/image/entities/image.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/user/entities/user.entity';
import { Interaction } from 'src/interaction/entities/interaction.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Pet, Image, User, Interaction])],
  controllers: [PetController],
  providers: [PetService],
})
export class PetModule {}
