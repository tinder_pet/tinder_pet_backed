import { User } from 'src/user/entities/user.entity';
import { Image } from 'src/image/entities/image.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Pet {
  @PrimaryGeneratedColumn()
  petID: number;

  @Column()
  petName: string;

  @Column()
  sex: string;
  @Column()
  species: string; // cat & Dog
  @Column('float')
  age: number;

  @Column('float')
  weight: number;

  @Column()
  color: string;

  @Column()
  furLength: string;

  @Column()
  breed: string;

  @Column()
  petDes: string;

  @Column()
  vaccine: string;

  @Column()
  foodsEaten: string;

  @Column()
  parentingStatus: string;

  @Column()
  sterileState: string;

  @Column()
  healthHistory: string;

  @Column()
  specialBehaviors: string;

  @Column()
  objective: string;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => User, (user) => user.pets)
  user: User;

  @OneToMany(() => Image, (image) => image.pet)
  images: Image[];

  // @OneToMany(() => Interaction, (interaction) => interaction.pets)
  // interactions: Interaction[];
}
