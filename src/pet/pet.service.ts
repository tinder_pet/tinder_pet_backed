import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePetDto } from './dto/create-pet.dto';
import { UpdatePetDto } from './dto/update-pet.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Pet } from './entities/pet.entity';
import { Image } from 'src/image/entities/image.entity';
import { In, Not, Repository } from 'typeorm';
import { User } from 'src/user/entities/user.entity';
import { Interaction } from 'src/interaction/entities/interaction.entity';
import axios from 'axios';
import { PetPayload } from './entities/patpayload';

@Injectable()
export class PetService {
  constructor(
    @InjectRepository(Pet)
    private petsRepository: Repository<Pet>,
    @InjectRepository(Image)
    private imagesRepository: Repository<Image>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Interaction)
    private interactionRepository: Repository<Interaction>,
  ) {}

  async create(createPetDto: CreatePetDto) {
    const { images, userID, ...petData } = createPetDto;

    // ค้นหาผู้ใช้จาก userID
    const user = await this.usersRepository.findOne({
      where: { userID },
    });

    if (!userID) {
      throw new NotFoundException('userID must be provided');
    }

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const pet = this.petsRepository.create({
      ...petData,
      user,
    });

    const savedPet = await this.petsRepository.save(pet);

    if (!savedPet) {
      throw new NotFoundException('Pet could not be created');
    }

    if (Array.isArray(images) && images.length > 0) {
      const imageEntities = images.map((imageUrl) => {
        return this.imagesRepository.create({ image: imageUrl, pet: savedPet });
      });

      await this.imagesRepository.save(imageEntities);
    }

    return savedPet;
  }

  findAll() {
    return this.petsRepository.find({
      relations: ['images', 'user'],
    });
  }

  async findOne(id: number) {
    const pet = await this.petsRepository.findOne({
      where: { petID: id },
      relations: ['images', 'user'],
    });
    if (!pet) {
      throw new NotFoundException();
    }
    return pet;
  }

  async update(id: number, updatePetDto: UpdatePetDto) {
    const pet = await this.petsRepository.findOne({
      where: { petID: id },
    });

    if (!pet) {
      throw new NotFoundException();
    }

    const { images, ...petData } = updatePetDto;

    this.petsRepository.merge(pet, petData);

    await this.petsRepository.save(pet);

    return pet;
  }

  async remove(id: number) {
    const pet = await this.petsRepository.findOneBy({ petID: id });
    if (!pet) {
      throw new NotFoundException();
    }
    return this.petsRepository.softRemove(pet);
  }

  private calculateDistance(
    lat1: number,
    lon1: number,
    lat2: number,
    lon2: number,
  ): number {
    const R = 6371;
    const dLat = this.degreesToRadians(lat2 - lat1);
    const dLon = this.degreesToRadians(lon2 - lon1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.degreesToRadians(lat1)) *
        Math.cos(this.degreesToRadians(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
  }

  private degreesToRadians(degrees: number): number {
    return degrees * (Math.PI / 180);
  }
  private formatDistance(distance: number): string {
    return distance.toFixed(2) + ' km';
  }

  async findPetsByUserId(userId: number): Promise<Pet[]> {
    const pets = await this.petsRepository.find({
      where: { user: { userID: userId } },
    });

    if (!pets || pets.length === 0) {
      throw new NotFoundException('No pets found for this user');
    }

    return pets;
  }

  async findNearbySameSpeciesPets(
    userId: number,
  ): Promise<{ pet: Pet; distance: string }[]> {
    const user = await this.usersRepository.findOne({
      where: { userID: userId },
      relations: ['pets'],
    });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const userLat = user.latitude;
    const userLon = user.longitude;
    const userPets = user.pets;

    if (userPets.length === 0) {
      throw new NotFoundException('User has no pets');
    }

    const species = userPets[0].species;
    const userFirstPetId = userPets[0].petID;

    const interactions = await this.interactionRepository.find({
      where: { pet1: { petID: userFirstPetId } },
      relations: ['pet2'],
    });

    const interactedPetIds = interactions.map(
      (interaction) => interaction.pet2.petID,
    );

    const allPets = await this.petsRepository.find({
      where: {
        species: species,
        user: Not(userId),
        petID: Not(In(interactedPetIds)),
      },
      relations: ['user', 'images'],
    });

    const nearbyPetsWithDistance = allPets
      .map((pet) => {
        const petLat = pet.user.latitude;
        const petLon = pet.user.longitude;
        const distance = this.calculateDistance(
          userLat,
          userLon,
          petLat,
          petLon,
        );
        return { pet, distance };
      })
      .filter(({ distance }) => distance <= 20)
      .map(({ pet, distance }) => ({
        pet,
        distance: this.formatDistance(distance),
      }));

    return nearbyPetsWithDistance;
  }
  async findNearbyCompatiblePets(
    userId: number,
    weights: {
      breed?: number;
      color?: number;
      age?: number;
      weight?: number;
      furLength?: number;
      behavior?: number;
    } = {},
  ): Promise<{ pet: Pet; distance: string; compatibility: number }[]> {
    const user = await this.usersRepository.findOne({
      where: { userID: userId },
      relations: ['pets', 'pets.user'],
    });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const userPets = user.pets;

    if (userPets.length === 0) {
      throw new NotFoundException('User has no pets');
    }

    const userFirstPet = userPets[0];

    const userLat = Number(user.latitude);
    const userLon = Number(user.longitude);

    if (isNaN(userLat) || isNaN(userLon)) {
      throw new Error('User latitude or longitude is invalid');
    }

    const species = userFirstPet.species;
    const userFirstPetId = userPets[0].petID;
    const interactions = await this.interactionRepository.find({
      where: { pet1: { petID: userFirstPetId } },
      relations: ['pet2'],
    });

    const interactedPetIds = interactions.map(
      (interaction) => interaction.pet2.petID,
    );

    const allPets = await this.petsRepository.find({
      where: {
        species: species,
        user: Not(userId),
        petID: Not(In(interactedPetIds)),
      },
      relations: ['user', 'images'],
    });

    const nearbyPetsWithDistance = allPets.map((pet) => {
      if (pet.user) {
        const petLat = Number(pet.user.latitude);
        const petLon = Number(pet.user.longitude);

        if (isNaN(petLat) || isNaN(petLon)) {
          throw new Error('Pet user data is invalid');
        }

        const distance = this.calculateDistance(
          userLat,
          userLon,
          petLat,
          petLon,
        );
        return { pet, distance };
      }
      throw new Error('Pet user data is undefined');
    });

    const payload: PetPayload[] = [
      {
        petID: userFirstPet.petID,
        petName: userFirstPet.petName,
        sex: userFirstPet.sex,
        species: userFirstPet.species,
        age: userFirstPet.age,
        weight: userFirstPet.weight,
        color: userFirstPet.color,
        furLength: userFirstPet.furLength,
        breed: userFirstPet.breed,
        petDes: userFirstPet.petDes,
        vaccine: userFirstPet.vaccine,
        foodsEaten: userFirstPet.foodsEaten,
        parentingStatus: userFirstPet.parentingStatus,
        sterileState: userFirstPet.sterileState,
        healthHistory: userFirstPet.healthHistory,
        specialBehaviors: userFirstPet.specialBehaviors,
        objective: userFirstPet.objective,
        deletedAt: userFirstPet.deletedAt,
      },
      ...nearbyPetsWithDistance.map(({ pet }) => ({
        petID: pet.petID,
        petName: pet.petName,
        sex: pet.sex,
        species: pet.species,
        age: pet.age,
        weight: pet.weight,
        color: pet.color,
        furLength: pet.furLength,
        breed: pet.breed,
        petDes: pet.petDes,
        vaccine: pet.vaccine,
        foodsEaten: pet.foodsEaten,
        parentingStatus: pet.parentingStatus,
        sterileState: pet.sterileState,
        healthHistory: pet.healthHistory,
        specialBehaviors: pet.specialBehaviors,
        objective: pet.objective,
        deletedAt: pet.deletedAt,
      })),
    ];

    console.log(payload);
    console.log(weights);

    const compatibilityResponse = await this.calculateCompatibility(
      payload,
      weights,
    );

    return nearbyPetsWithDistance.map(({ pet }) => {
      const compatibilityData = compatibilityResponse.find(
        (item) => item.petID === pet.petID.toString(),
      );

      const distanceValue = this.calculateDistance(
        userFirstPet.user.latitude,
        userFirstPet.user.longitude,
        pet.user.latitude,
        pet.user.longitude,
      );

      return {
        pet,
        distance: this.formatDistance(distanceValue),
        compatibility: compatibilityData
          ? compatibilityData['Compatibility (%)'] || 0
          : 0,
      };
    });
  }

  private async calculateCompatibility(
    payload: PetPayload[],
    weights: any,
  ): Promise<any[]> {
    const payloadToSend = {
      pets: payload.map((pet) => ({
        petID: String(pet.petID),
        petName: pet.petName,
        species: pet.species,
        age: String(pet.age),
        weight: String(pet.weight),
        color: pet.color,
        furLength: pet.furLength,
        breed: pet.breed,
        petDes: pet.petDes,
        specialBehaviors: pet.specialBehaviors,
      })),
      weights: {
        breed: weights.breed || 0.4,
        color: weights.color || 0.1,
        age: weights.age || 0.1,
        weight: weights.weight || 0.1,
        furLength: weights.furLength || 0.1,
        behavior: weights.behavior || 0.2,
      },
    };
    try {
      const response = await axios.post(
        'http://127.0.0.1:5000/calculate_compatibility/',
        payloadToSend,
      );
      return response.data;
    } catch (error) {
      console.error(error.response.data);
      throw error;
    }
  }
}
