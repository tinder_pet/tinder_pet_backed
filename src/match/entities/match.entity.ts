import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';
import { Pet } from 'src/pet/entities/pet.entity';

@Entity()
export class Match {
  @PrimaryGeneratedColumn()
  matchID: number;

  @ManyToOne(() => Pet)
  pet1: Pet;

  @ManyToOne(() => Pet)
  pet2: Pet;

  @CreateDateColumn()
  createMatch: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
