import { ChatRoom } from 'src/chatroom/entities/chatroom.entity';
import { Member } from 'src/member/entities/member.entity';
import { Message } from 'src/message/entities/message.entity';
import { Pet } from 'src/pet/entities/pet.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  userID: number;

  @Column()
  userName: string;

  @Column()
  password: string;

  @Column()
  email: string;

  @Column()
  sex: string;

  @Column({ type: 'char', length: 10 })
  phoneNumber: string;

  @Column()
  userDes: string;

  @Column()
  statusMember: string;

  @Column()
  userImage: string;

  @Column()
  contact: string;

  @Column()
  age: number;

  @Column()
  nationality: string;

  @Column()
  religion: string;

  @Column()
  address: string;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Pet, (pet) => pet.user)
  pets: Pet[];

  @OneToMany(() => Member, (member) => member.user)
  members: Member[];

  @Column({ type: 'decimal', precision: 10, scale: 7, nullable: true })
  latitude: number;

  @Column({ type: 'decimal', precision: 10, scale: 7, nullable: true })
  longitude: number;

  @OneToMany(() => ChatRoom, (chatRoom) => chatRoom.user1)
  chatRooms: ChatRoom[];

  @OneToMany(() => ChatRoom, (chatRoom) => chatRoom.user2)
  chatRoomsAsSecondUser: ChatRoom[];

  @OneToMany(() => Message, (message) => message.sender)
  messages: Message[];
}
