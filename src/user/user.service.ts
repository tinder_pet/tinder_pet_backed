import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createUserDto.password, salt);
    createUserDto.password = hash;
    const user = this.usersRepository.save(createUserDto);
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  findAll() {
    return this.usersRepository.find({ relations: ['pets', 'members'] });
  }
  async getLocation(
    userID: number,
  ): Promise<{ latitude: number; longitude: number }> {
    // ค้นหาผู้ใช้จาก userID
    const user = await this.usersRepository.findOne({ where: { userID } });

    if (!user) {
      throw new NotFoundException(`User with ID ${userID} not found`);
    }

    return {
      latitude: user.latitude,
      longitude: user.longitude,
    };
  }

  async findOne(id: number) {
    const user = await this.usersRepository.findOne({
      where: { userID: id },
      relations: ['pets', 'members'],
    });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user;
  }

  async findOneByEmail(email: string) {
    const user = await this.usersRepository.findOne({
      where: { email: email },
      relations: ['pets', 'members', 'pets.images'],
    });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.findOneBy({ userID: id });
    if (updateUserDto.password !== undefined) {
      const salt = await bcrypt.genSalt();
      const hash = await bcrypt.hash(updateUserDto.password, salt);
      updateUserDto.password = hash;
    }
    if (!user) {
      throw new NotFoundException();
    }
    const updatedUser = { ...user, ...updateUserDto };
    return this.usersRepository.save(updatedUser);
  }
  async updateLocation(
    userID: number,
    latitude: number,
    longitude: number,
  ): Promise<User> {
    // ค้นหาผู้ใช้จาก userID
    const user = await this.usersRepository.findOne({ where: { userID } });

    if (!user) {
      throw new NotFoundException(`User with ID ${userID} not found`);
    }

    user.latitude = latitude;
    user.longitude = longitude;

    await this.usersRepository.save(user);

    return user;
  }

  async remove(id: number) {
    const user = await this.usersRepository.findOneBy({ userID: id });
    if (!user) {
      throw new NotFoundException();
    }
    return this.usersRepository.softRemove(user);
  }
}
