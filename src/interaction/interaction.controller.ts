import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { InteractionService } from './interaction.service';
import { CreateInteractionDto } from './dto/create-interaction.dto';
import { InteractionResponse } from './interaction.service';
@Controller('interaction')
export class InteractionController {
  constructor(private readonly interactionService: InteractionService) {}

  @Post()
  async createInteraction(
    @Body() createInteractionDto: CreateInteractionDto,
  ): Promise<InteractionResponse> {
    return this.interactionService.createInteraction(createInteractionDto);
  }

  @Get()
  findAll() {
    return this.interactionService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.interactionService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.interactionService.remove(+id);
  }
}
