import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateInteractionDto } from './dto/create-interaction.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Interaction } from './entities/interaction.entity';
import { Repository } from 'typeorm';
import { Pet } from 'src/pet/entities/pet.entity';
import { Match } from 'src/match/entities/match.entity';
import { ChatRoom } from 'src/chatroom/entities/chatroom.entity';

export interface InteractionResponse {
  message: string;
  interaction: Interaction;
}

@Injectable()
export class InteractionService {
  constructor(
    @InjectRepository(Interaction)
    private interactionsRepository: Repository<Interaction>,
    @InjectRepository(Pet)
    private readonly petRepository: Repository<Pet>,
    @InjectRepository(Match)
    private readonly matchRepository: Repository<Match>,
    @InjectRepository(ChatRoom)
    private readonly chatRoomRepository: Repository<ChatRoom>,
  ) {}

  async createInteraction(
    createInteractionDto: CreateInteractionDto,
  ): Promise<InteractionResponse> {
    const { isLiked, pet1, pet2 } = createInteractionDto;

    if (pet1 === pet2) {
      throw new ConflictException('Cannot interact with the same pet');
    }

    const pet1Entity = await this.petRepository.findOne({
      where: { petID: pet1 },
      relations: ['user'],
    });
    const pet2Entity = await this.petRepository.findOne({
      where: { petID: pet2 },
      relations: ['user'],
    });

    if (!pet1Entity || !pet2Entity) {
      throw new NotFoundException('One or both pets not found');
    }

    const existingInteraction = await this.interactionsRepository.findOne({
      where: { pet1: pet1Entity, pet2: pet2Entity },
    });

    if (existingInteraction) {
      existingInteraction.isLiked = isLiked;
      await this.interactionsRepository.save(existingInteraction);
      return {
        message: 'Interaction updated successfully',
        interaction: existingInteraction,
      };
    }

    const interaction = new Interaction();
    interaction.pet1 = pet1Entity;
    interaction.pet2 = pet2Entity;
    interaction.isLiked = isLiked;

    const savedInteraction =
      await this.interactionsRepository.save(interaction);

    if (isLiked) {
      const reciprocalInteraction = await this.interactionsRepository.findOne({
        where: { pet1: pet2Entity, pet2: pet1Entity },
      });

      if (reciprocalInteraction && reciprocalInteraction.isLiked) {
        const existingMatch = await this.matchRepository.findOne({
          where: [
            { pet1: pet1Entity, pet2: pet2Entity },
            { pet1: pet2Entity, pet2: pet1Entity },
          ],
        });

        if (existingMatch) {
          throw new ConflictException('This match already exists');
        }

        const match = new Match();
        match.pet1 = pet1Entity;
        match.pet2 = pet2Entity;

        await this.matchRepository.save(match);

        const chatRoom = new ChatRoom();
        chatRoom.user1 = pet1Entity.user;
        chatRoom.user2 = pet2Entity.user;
        await this.chatRoomRepository.save(chatRoom);

        return {
          message: 'Match created successfully',
          interaction: savedInteraction,
        };
      }
    }

    return {
      message: 'Interaction created successfully',
      interaction: savedInteraction,
    };
  }

  findAll() {
    return this.interactionsRepository.find();
  }

  async findOne(id: number) {
    const interaction = await this.interactionsRepository.findOne({
      where: { interactionID: id },
    });
    if (!interaction) {
      throw new NotFoundException();
    }
    return interaction;
  }

  async remove(id: number) {
    const interaction = await this.interactionsRepository.findOneBy({
      interactionID: id,
    });
    if (!interaction) {
      throw new NotFoundException();
    }
    return this.interactionsRepository.softRemove(interaction);
  }
}
