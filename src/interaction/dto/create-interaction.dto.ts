import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class CreateInteractionDto {
  @IsNotEmpty()
  @IsString()
  isLiked: boolean;

  @IsNotEmpty()
  @IsInt()
  pet1: number;

  @IsNotEmpty()
  @IsInt()
  pet2: number;
}
