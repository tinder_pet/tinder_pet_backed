import { Module } from '@nestjs/common';
import { InteractionService } from './interaction.service';
import { InteractionController } from './interaction.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Interaction } from './entities/interaction.entity';
import { Pet } from 'src/pet/entities/pet.entity';
import { Match } from 'src/match/entities/match.entity';
import { ChatRoom } from 'src/chatroom/entities/chatroom.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Interaction, Pet, Match, ChatRoom])],
  controllers: [InteractionController],
  providers: [InteractionService],
})
export class InteractionModule {}
