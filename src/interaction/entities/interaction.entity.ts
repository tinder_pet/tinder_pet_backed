import { Pet } from 'src/pet/entities/pet.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
  ManyToOne,
  Unique,
} from 'typeorm';

@Entity()
@Unique(['pet1', 'pet2'])
export class Interaction {
  @PrimaryGeneratedColumn()
  interactionID: number;

  @Column({ type: 'boolean', default: false })
  isLiked: boolean; // true = liked, false = not liked

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Pet, { eager: true })
  pet1: Pet;

  @ManyToOne(() => Pet, { eager: true })
  pet2: Pet;
}
