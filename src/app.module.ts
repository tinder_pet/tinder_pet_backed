import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ImageModule } from './image/image.module';
import { PetModule } from './pet/pet.module';
import { InteractionModule } from './interaction/interaction.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Image } from './image/entities/image.entity';
import { Pet } from './pet/entities/pet.entity';
import { Interaction } from './interaction/entities/interaction.entity';
import { Match } from './match/entities/match.entity';
import { MatchModule } from './match/match.module';
import { UserModule } from './user/user.module';
import { User } from './user/entities/user.entity';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';
import { ChatroomModule } from './chatroom/chatroom.module';
import { MessageModule } from './message/message.module';
import { Message } from './message/entities/message.entity';
import { AuthModule } from './auth/auth.module';
import { ChatRoom } from './chatroom/entities/chatroom.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'pawspair',
      entities: [
        Image,
        Pet,
        Interaction,
        Match,
        User,
        Member,
        ChatRoom,
        Message,
      ],
      synchronize: true,
    }),
    ImageModule,
    PetModule,
    InteractionModule,
    MatchModule,
    UserModule,
    MemberModule,
    ChatroomModule,
    MessageModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
