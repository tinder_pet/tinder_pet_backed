import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UploadedFile,
  UseInterceptors,
  Res,
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ImageService } from './image.service';
import { CreateImageDto } from './dto/create-image.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { diskStorage } from 'multer';
import { Response } from 'express';
import { Image } from 'src/image/entities/image.entity';

@Controller('image')
export class ImageController {
  constructor(private readonly imageService: ImageService) {}
  @Get('pet/:petId/first')
  async getFirstImageByPetId(
    @Param('petId') petId: number,
  ): Promise<Image | null> {
    const image = await this.imageService.getFirstImageByPetId(petId);
    if (!image) {
      throw new NotFoundException('No image found for this pet');
    }
    return image;
  }
  @Post(':petId')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './pet_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          const fileExtName = extname(file.originalname);
          cb(null, `${name}${fileExtName}`);
        },
      }),
    }),
  )
  async addImageToPet(
    @Param('petId') petId: number,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Image> {
    if (!file) {
      throw new BadRequestException('No file uploaded');
    }

    const imageData: Partial<Image> = {
      image: file.filename,
    };

    try {
      return this.imageService.addImageToPet(petId, imageData);
    } catch (error) {
      throw new InternalServerErrorException('Error adding image to pet');
    }
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './pet_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          const fileExtName = extname(file.originalname);
          cb(null, `${name}${fileExtName}`);
        },
      }),
    }),
  )
  async create(
    @Body() createImageDto: CreateImageDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (!file) {
      throw new BadRequestException('No file uploaded');
    }

    createImageDto.image = file.filename;

    try {
      return await this.imageService.create(createImageDto);
    } catch (error) {
      throw new InternalServerErrorException('Error creating image');
    }
  }

  @Get()
  findAll() {
    return this.imageService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.imageService.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const imagePet = await this.imageService.findOne(+id);
    if (!imagePet) {
      throw new BadRequestException('Image not found');
    }
    res.sendFile(imagePet.image, { root: './pet_image' });
  }

  @Get('image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './pet_image' });
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './pet_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          const fileExtName = extname(file.originalname);
          cb(null, `${name}${fileExtName}`);
        },
      }),
    }),
  )
  async updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (!file) {
      throw new BadRequestException('No file uploaded');
    }
    try {
      return this.imageService.update(+id, { image: file.filename });
    } catch (error) {
      throw new InternalServerErrorException('Error updating image');
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      return await this.imageService.remove(+id);
    } catch (error) {
      throw new InternalServerErrorException('Error removing image');
    }
  }
}
