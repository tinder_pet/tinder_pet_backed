import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateImageDto } from './dto/create-image.dto';
import { UpdateImageDto } from './dto/update-image.dto';
import { Image } from './entities/image.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Pet } from 'src/pet/entities/pet.entity';

@Injectable()
export class ImageService {
  constructor(
    @InjectRepository(Pet)
    private readonly petsRepository: Repository<Pet>,
    @InjectRepository(Image)
    private readonly imagesRepository: Repository<Image>,
  ) {}
  async getFirstImageByPetId(petId: number): Promise<Image | null> {
    const pet = await this.petsRepository.findOne({
      where: { petID: petId },
      relations: ['images'],
    });

    if (!pet) {
      throw new NotFoundException('Pet not found');
    }

    if (pet.images.length > 0) {
      return pet.images[0];
    } else {
      return null;
    }
  }
  async addImageToPet(
    petId: number,
    imageData: Partial<Image>,
  ): Promise<Image> {
    const pet = await this.petsRepository.findOne({
      where: { petID: petId },
    });
    if (!pet) {
      throw new Error('Pet not found');
    }

    const image = this.imagesRepository.create({
      ...imageData,
      pet: pet,
    });

    return this.imagesRepository.save(image);
  }

  create(createImageDto: CreateImageDto) {
    const image = this.imagesRepository.save(createImageDto);
    if (!image) {
      throw new NotFoundException();
    }
    return image;
  }

  findAll() {
    return this.imagesRepository.find({ relations: ['pet'] });
  }

  async findOne(id: number) {
    const image = await this.imagesRepository.findOne({
      where: { imageID: id },
    });
    if (!image) {
      throw new NotFoundException();
    }
    return image;
  }

  async update(id: number, updateImageDto: UpdateImageDto) {
    const image = await this.imagesRepository.findOneBy({ imageID: id });
    if (!image) {
      throw new NotFoundException();
    }
    const updatedImage = { ...image, ...updateImageDto };
    return this.imagesRepository.save(updatedImage);
  }

  async remove(id: number) {
    const image = await this.imagesRepository.findOneBy({ imageID: id });
    if (!image) {
      throw new NotFoundException();
    }
    return this.imagesRepository.softRemove(image);
  }
}
