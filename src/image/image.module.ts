import { Module } from '@nestjs/common';
import { ImageService } from './image.service';
import { ImageController } from './image.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Image } from './entities/image.entity';
import { Pet } from 'src/pet/entities/pet.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Image, Pet])],
  controllers: [ImageController],
  providers: [ImageService],
})
export class ImageModule {}
