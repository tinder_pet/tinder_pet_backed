import { Pet } from 'src/pet/entities/pet.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Image {
  @PrimaryGeneratedColumn()
  imageID: number;

  @Column()
  image: string;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Pet, (pet) => pet.images)
  pet: Pet;
}
