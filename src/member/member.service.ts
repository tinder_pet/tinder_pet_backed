import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Member } from './entities/member.entity';
import { Repository } from 'typeorm';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private membersRepository: Repository<Member>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createMemberDto: CreateMemberDto) {
    const user = await this.usersRepository.findOne({
      where: { userID: createMemberDto.userID },
    });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (user.statusMember === 'active') {
      throw new BadRequestException(
        'Cannot create new member. User is already a member.',
      );
    }
    const newMember = this.membersRepository.create(
      Object.assign({}, createMemberDto, { user }),
    );
    await this.membersRepository.save(newMember);
    user.statusMember = 'active';
    await this.usersRepository.save(user);

    return newMember;
  }

  findAll() {
    return this.membersRepository.find({
      relations: ['user'],
    });
  }

  findMembersByUserId(userId: number) {
    return this.membersRepository.find({
      where: { user: { userID: userId } },
      order: {
        createdAt: 'DESC', // หรือ 'DESC' ถ้าต้องการเรียงจากใหม่ไปเก่า
      },
    });
  }

  async findOne(id: number) {
    const member = await this.membersRepository.findOne({
      where: { memberID: id },
    });
    if (!member) {
      throw new NotFoundException();
    }
    return member;
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const member = await this.membersRepository.findOneBy({ memberID: id });
    if (!member) {
      throw new NotFoundException();
    }
    const updatedMember = { ...member, ...updateMemberDto };
    return this.membersRepository.save(updatedMember);
  }

  async remove(id: number) {
    const member = await this.membersRepository.findOneBy({ memberID: id });
    if (!member) {
      throw new NotFoundException();
    }
    return this.membersRepository.softRemove(member);
  }
}
