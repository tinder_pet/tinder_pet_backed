import { User } from 'src/user/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  BeforeInsert,
} from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  memberID: number;
  @Column()
  totalPrice: number;

  @Column()
  payment: string;

  @Column()
  endDate: Date;

  @CreateDateColumn()
  createdAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @BeforeInsert()
  setEndDate() {
    const currentDate = new Date();
    this.endDate = new Date(currentDate.setDate(currentDate.getDate() + 30));
  }

  @ManyToOne(() => User, (user) => user.members)
  user: User;
}
