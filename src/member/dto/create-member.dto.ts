import { IsNotEmpty, IsInt } from 'class-validator';

export class CreateMemberDto {
  @IsNotEmpty()
  @IsInt()
  userID: number;
}
